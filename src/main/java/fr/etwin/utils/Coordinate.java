package fr.etwin.utils;

import java.util.Objects;

public class Coordinate {
    public int mX;
    public int mY;
    
    public Coordinate(int mX, int mY) {
        this.mX = mX;
        this.mY = mY;
    }

    public int hashCode() {
        return Objects.hash(mX, mY);
    }

    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        else if (!(obj instanceof Coordinate))
            return false;

        Coordinate that = (Coordinate) obj;
        return this.mX == that.mX && this.mY == that.mY;
    }

    public String toString() {
        return "(" + mX + ", " + mY + ")";
    }
}